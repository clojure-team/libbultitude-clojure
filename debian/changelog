libbultitude-clojure (0.3.0-3) UNRELEASED; urgency=low

  * Trim trailing whitespace.
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Add debian/watch file, using github.
  * Remove unnecessary get-orig-source-target.
  * Update standards version to 4.5.0, no changes needed.
  [ Louis-Philippe Véronneau ]
  * d/control: New email for the Clojure Team.



 -- Debian Janitor <janitor@jelmer.uk>  Wed, 10 Jun 2020 04:15:17 -0000

libbultitude-clojure (0.3.0-2) unstable; urgency=medium

  * Update POM with new version info.

 -- Elana Hashman <ehashman@debian.org>  Sun, 10 Feb 2019 22:03:24 -0500

libbultitude-clojure (0.3.0-1) unstable; urgency=medium

  * New upstream release: following a community-accepted fork as the
    original author has died.
  * Assert compliance with new Standards-Version.

 -- Elana Hashman <ehashman@debian.org>  Mon, 04 Feb 2019 22:44:27 -0500

libbultitude-clojure (0.2.8-4) unstable; urgency=medium

  * Move Vcs to salsa.
  * Remove watch file.
  * Clean up unneeded rules vars and classpath file.

 -- Elana Hashman <ehashman@debian.org>  Mon, 19 Mar 2018 12:10:12 -0400

libbultitude-clojure (0.2.8-3) unstable; urgency=medium

  [ Tom Marble ]
  * Change libclojure from Build-Dep to Dep
  * Declare compliance with Debian Policy 4.1.3.

  [ Elana Hashman ]
  * Remove Daigo from Uploaders (Closes: #863766)
  * Switch maintainer to Clojure Team from Java Team.
  * Remove markdown->html doc generation.
  * Move runtime deps out of builddeps.
  * Add autopkgtests.
  * Fix my email...

 -- Elana Hashman <ehashman@debian.org>  Sat, 17 Mar 2018 23:05:23 -0400

libbultitude-clojure (0.2.8-2) unstable; urgency=medium

  * Upload to unstable.
  * Declare compliance with Debian Policy 4.0.0.

 -- Elana Hashman <debian@hashman.ca>  Sat, 24 Jun 2017 22:27:54 -0400

libbultitude-clojure (0.2.8-1) experimental; urgency=medium

  * Team upload.

  [ Elana Hashman ]
  * New upstream release. (Closes: #852244)
  * Added maven packaging.

  [ Markus Koschany ]
  * debian/copyright: Fix copyright incompatibility between GPL and EPL
    licensed files.
  * Use canonical Vcs address.
  * Fix wrong debian file names. Should be either libbultitude-clojure.* or
    just the file name without the package name prefix. libbultitude does not
    exist though.
  * Don't install README.md twice.
  * Drop jlibs file. Not needed because we use maven-repo-helper already.
  * Build with maven_repo_helper instead of jh_maven_repo_helper.

 -- Markus Koschany <apo@debian.org>  Sun, 14 May 2017 00:17:51 +0200

libbultitude-clojure (0.2.7-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
  * Build depend on clojure (>= 1.8)
  * Standards-Version updated to 3.9.8
  * Switch to debhelper level 10
  * Use secure Vcs-* URLs
  * Fixed debian/watch

 -- Emmanuel Bourg <ebourg@apache.org>  Fri, 23 Dec 2016 11:50:13 +0100

libbultitude-clojure (0.2.0-2) unstable; urgency=medium

  * Team upload.
  * Build depend on clojure1.6
  * Standards-Version updated to 3.9.5 (no changes)
  * Switch to debhelper level 9

 -- Emmanuel Bourg <ebourg@apache.org>  Fri, 12 Sep 2014 22:44:26 +0200

libbultitude-clojure (0.2.0-1) unstable; urgency=low

  * Initial release (Closes: #698690)

 -- Daigo Moriwaki <daigo@debian.org>  Tue, 22 Jan 2013 13:06:56 +0900
